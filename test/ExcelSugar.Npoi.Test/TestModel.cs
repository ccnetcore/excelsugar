﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelSugar.Core.Attributes;
using ExcelSugar.Core;

namespace ExcelSugar.Npoi.Test
{

    [SugarSheet("测试表")]
    public class TestModel
    {
        [SugarHead("姓名")]
        public string Name { get; set; }
        [SugarHead("描述")]
        public string Description { get; set; }

        /// <summary>
        /// 导出可支持动态列模型
        /// </summary>
        [SugarDynamicHead]
        public List<DynamicModel> DynamicModels { get; set; } = new List<DynamicModel>();

        [SugarHead("字典列", IsJson = true)]
        public Dictionary<int, string> Dic { get; set; } = new Dictionary<int, string>();

        [SugarHead("对象列", IsJson = true)]

        public ObjectModel ObjectModel { get; set; } = new ObjectModel();
    }
    /// <summary>
    /// 动态表头，需具备IsCode、IsValue、IsName 3个字段
    /// </summary>
    public class DynamicModel
    {
        [SugarDynamicHead(IsCode = true)]
        public string DataCode { get; set; }

        [SugarDynamicHead(IsValue = true)]
        public int DataValue { get; set; }

        [SugarDynamicHead(IsName = true)]
        public string DataName { get; set; }
    }


    public class ObjectModel
    {
        public string Test1 { get; set; }
        public int Test2 { get; set; }
        public List<string> Test3 { get; set; } = new List<string>();
        public List<int> Test4 { get; set; }
    }
}

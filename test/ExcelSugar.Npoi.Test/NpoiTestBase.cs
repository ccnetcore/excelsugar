using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelSugar.Core;
using ExcelSugar.Core.Converter;

namespace ExcelSugar.Npoi.Test
{
    public class NpoiTestBase
    {

        protected IExcelSugarClient CreateClient()
        {
            //创建客户端
            IExcelSugarClient excelSugarClient = new ExcelSugarClient(new ExcelSugarConfig { Path = "../../../TempExcel/Test.xlsx", HandlerType = ExcelHandlerType.Npoi, CellValueConverter = new JsonCellValueConver() });
            //支持动态列模型
            return excelSugarClient;
        }

        protected List<TestModel> CreateNullTestModel()
        {
            return new List<TestModel>();
        }

        protected List<TestModel> CreateTestModel()
        {
            var entities = new List<TestModel> {
                new TestModel { Description = "男的", Name = "张三",
                    Dic=new Dictionary<int, string>{ { 10,"OK"},{20,"NO" } },
                    ObjectModel=new ObjectModel{ Test1="1",Test2=2,Test3=new List<string>{ "1","2","3"},Test4=new List<int>{ 6,7,8,9} },
                    DynamicModels = new List<DynamicModel>{
                        new DynamicModel { DataCode="height",DataName="身高",DataValue=188},
                } },
                new TestModel { Description = "女的", Name = "李四",
                    ObjectModel=new ObjectModel{ Test1="11",Test2=222,Test3=new List<string>{ "11","22","33"},Test4=new List<int>{ 66,77,88,99} },
                    DynamicModels=new List<DynamicModel>{
                        new DynamicModel { DataCode="height",DataName="身高",DataValue=168},
                        new DynamicModel { DataCode="age",DataName="年龄",DataValue=18},
                } }
            };
            return entities;
        }
    }
}

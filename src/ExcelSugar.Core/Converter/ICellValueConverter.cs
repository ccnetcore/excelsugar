﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ExcelSugar.Core
{
    public interface ICellValueConverter
    {
        object? CellToProperty(string cellValue, PropertyInfo propertyInfo);
        string PropertyToCell(object? propertyValue, PropertyInfo propertyInfo);
    }
}
